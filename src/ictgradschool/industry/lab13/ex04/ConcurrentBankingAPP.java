package ictgradschool.industry.lab13.ex04;

import com.sun.org.apache.bcel.internal.generic.NEW;
import sun.security.ssl.SSLContextImpl;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class ConcurrentBankingAPP extends SerialBankingApp {


    //CREATING BLOCKING OBJECT

    private final ArrayBlockingQueue<Transaction> queue = new ArrayBlockingQueue<Transaction> (10,true);

    // Create BankAccount object to operate on.

    BankAccount account = new BankAccount ();

    List<Transaction> transactions = TransactionGenerator.readDataFile ();



    Thread producer = new Thread (new Runnable () {
        @Override
        public void run() {



            if (transactions!=null) {
                for (Transaction transaction : transactions) {
                    try {
                        queue.put (transaction);

                    } catch (InterruptedException e) {

                        e.printStackTrace ();
                    }
                }
            }
        }
    });

    Runnable consumer = new Runnable () {
        @Override
        public void run() {
            // Keep running until it is interrupted and the queue is empty
            while (!Thread.currentThread ().isInterrupted () && !(queue.isEmpty ())) {

                try {

                    queue.take ();

                    // For each Transaction, apply it to the BankAccount instance.
                    for(Transaction transaction : transactions) {
                        switch (transaction._type) {
                            case Deposit:
                                account.deposit(transaction._amountInCents);
                                break;
                            case Withdraw:
                                account.withdraw(transaction._amountInCents);
                                break;
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace ();
                }
            }
        }
    };


    Thread consumer1 = new Thread (consumer);
    Thread consumer2 = new Thread (consumer);


    // Acquire Transactions to process.


// Acquire Transactions to process.

//    List<Transaction> transactions = TransactionGenerator.readDataFile ();
//
//    // Create BankAccount object to operate on.
//
//    BankAccount account = new BankAccount ();

//    for (Transaction transaction : transactions) {
//        switch (transaction._type) {
//            case Deposit:
//                account.deposit (transaction._amountInCents);
//                transactionArrayBlockingQueue.add ()   ;
//                break;
//            case Withdraw:
//                account.withdraw (transaction._amountInCents);
//                break;
//        }
//    }


    // 4.terminate.


    // 5.Once the producer has finished, send an interrupt request to the two consumers. The
    //consumers should respond by terminating as soon as the queue becomes empty
    //after receiving the interrupt request. Note that if the consumers terminate because
    //the queue is empty before receiving the interrupt, there may be more data to arrive in
    //the queue that wouldn’t be processed.


    //6.Wait for the two consumers to terminate.

    //7.Print out the final balance of the shared ​BankAccount​ object.

    public void start() {
        producer.start ();
        consumer1.start ();
        consumer2.start ();

        try {
            // Wait for producer to finish
            producer.join ();

            // Tell the consumers to stop
            consumer1.interrupt ();
            consumer2.interrupt ();

            // Wait for the consumers to stop
            consumer1.join ();
            consumer2.join ();
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }




        // Print the final balance after applying all Transactions.
        System.out.println ("Final balance: " + account.getFormattedBalance ());

    }

    public static void main(String[] args) {

        //starting the threads

        new ConcurrentBankingAPP ().start ();

    }
}
