package ictgradschool.industry.lab13.ex03;

import jdk.nashorn.internal.ir.ReturnNode;
import sun.swing.BakedArrayList;

import javax.tools.JavaCompiler;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded  {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */

    private class MyRunnable implements Runnable {

        public double results;
        private final long iterations;

        public MyRunnable(long iterations) {
            this.iterations = iterations;
        }

        @Override
        public void run() {
            results = ExerciseThreeMultiThreaded.super.estimatePI (iterations);
        }
    }

    @Override
    protected double estimatePI(long numSamples) {
        numSamples = 1_999_999_999;
        final int numThreads = 10;

        List<MyRunnable> runs = new java.util.ArrayList<MyRunnable> ();
        List<Thread> threads = new ArrayList<> ();

        for (int i = 0; i < numThreads; i++) {
            MyRunnable r = new MyRunnable (numSamples / numThreads);
            Thread t = new Thread (r);
            runs.add (r);
            threads.add (t);
            t.start ();
        }

        double sum = 0;

        for (int i = 0; i < threads.size (); i++) {
            try {
                threads.get (i).join ();
                sum += runs.get (i).results;
            } catch (InterruptedException e) {
                e.printStackTrace ();
            }
        }

        return  sum / numThreads;
    }

    /** Program entry point. */
    public static void main(String[] args){
        (new ExerciseThreeMultiThreaded ()).start ();
    }
}



