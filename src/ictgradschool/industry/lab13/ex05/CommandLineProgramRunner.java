package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab13.ex04.SerialBankingApp;

import java.awt.*;

public class CommandLineProgramRunner {



    Thread commandLineThread = new Runnable () {


        @Override
        public void run() {
            System.out.println ("Please enter a value of N for which you want to calculate the prime numbers: ");

            long n = Long.parseLong (Keyboard.readInput ());

            PrimeFactorsTask task = new PrimeFactorsTask (n);

            System.out.println (task.getPrimeFactors ());


        }
    }



    public static void main(String[] args) {

        new CommandLineProgramRunner ().commandLineThread.start ();
        try {
            new CommandLineProgramRunner ().commandLineThread.join ();

        } catch (InterruptedException e) {
            e.printStackTrace ();
        }






    }
}
