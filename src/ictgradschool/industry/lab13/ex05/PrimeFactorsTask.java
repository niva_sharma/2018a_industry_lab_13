package ictgradschool.industry.lab13.ex05;

import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

import javax.swing.plaf.nimbus.State;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static ictgradschool.industry.lab13.ex05.TaskState.*;

public class PrimeFactorsTask implements Runnable {

    private long N;
    //instantiating the list
    List<Long> list;
    TaskState state;



    public PrimeFactorsTask(long n) {

        this.N = n;
        //instantiating the list
        state = Initialized;

        list = new List <Long> () {
            @Override
            public int size() {
                return list.size ();
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Long> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Long aLong) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Long> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Long> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Long get(int index) {
                return null;
            }

            @Override
            public Long set(int index, Long element) {
                return null;
            }

            @Override
            public void add(int index, Long element) {

            }

            @Override
            public Long remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Long> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Long> listIterator(int index) {
                return null;
            }


            @Override
            public List<Long> subList(int fromIndex, int toIndex) {
                return subList (0, list.size ());
            }

        };
    }


    public void run() {

        //algorithm for computing prime factors
        // for each potential factor
        for (long factor = 2; factor * factor <= N; factor++) {

            // if factor is a factor of n, repeatedly divide it out
            while (N % factor == 0) {
                System.out.print (factor + " ");
                list.add (factor);
                N = N / factor;
            }
        }

        // if biggest factor occurs only once, n > 1
        if (N > 1) {
           list.add (N);
        } else {
            System.out.println ();
        }
    }


    public long getN() {
        return N;
    }

    List<Long> getPrimeFactors() throws IllegalStateException {
        if(Thread.currentThread ().isInterrupted ()) {
            return list;
        }else {
            new IllegalStateException ();
        }
        return list;
    }

    public TaskState getState() {

        if (Thread.currentThread ().isInterrupted ()) {

        }
        return Completed;
    }


    public static void main(String[] args) {

//        Thread thread = new Thread (new PrimeFactorsTask (3));
//        thread.start ();
//        thread.getState ();
    }
}


